package main

// import "fmt"
//go get -u github.com/creamdog/gonfig
// import (
// 	"github.com/creamdog/gonfig"
// 	"os"
// )
//go get -u github.com/go-sql-driver/mysql
// import (
// 	"database/sql"
// 	_ "github.com/go-sql-driver/mysql"
// )
import (
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"./config"
	"./entities"
	"./models"
)

// import "strings"

// var (
// 	db_user_pwd ,db_url ,dp_owner ,db_type = "", "", "", ""
// 	db_select ,db_select_max ,db_delete_user = "", "", ""
// )

// type ExampleMessageStruct struct {
// 	Message string
// 	Subject string
// }

var (
	t          = time.Now()
	formatted  = ""
	configPath = ""
	logPath    = ""
)

func getCurrentTime() string {
	t = time.Now()
	//YYYYMMDDTHHMMSS
	formatted = fmt.Sprintf("%d%02d%02d%02d%02d%02d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
	return formatted
}
func getCurrentDate() string {
	t = time.Now()
	//YYYYMMDD
	formatted = fmt.Sprintf("%d%02d%02d", t.Year(), t.Month(), t.Day())
	return formatted
}

//Test 中文註解
func main_test() {

	//0-1.設定 log目錄
	configPath = os.Args[1]
	logPath = config.GetConfigValue(configPath, "dir/log/path")
	// logPath does not exist
	if _, err := os.Stat(logPath); os.IsNotExist(err) {
		os.MkdirAll(logPath, os.ModePerm)
	}

	//0-2.設定 Log File Setting
	var vCurrentDT = getCurrentDate()
	//O_RDONLY 	:唯讀/O_WRONLY :唯寫/O_RDWR: 讀寫
	//O_APPEND 	:寫入時使用附加方式
	//O_CREATE 	:檔案不存在時建立新檔
	//O_EXCL   	:與 O_CREATE 併用，檔案必須不存在
	//O_SYNC 	:以同步 I/O 開啟
	//O_TRUNC  	:檔案開啟時清空文件
	//perm 的話是檔案八進位權限，例如 0777；另外，還有個 os.Create，實現上就是使用 OpenFile 以 0666 的方式建立可讀寫的檔案（清空文件）：
	f, err := os.OpenFile(logPath+"/Timcard_log_"+vCurrentDT+".log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()
	log.SetOutput(f)

	//0-3.開啟DB連線
	db, err := config.GetMySQLDB(configPath)
	if err != nil {
		log.Println(err)
	} else {

		timecardModel := models.TimecardModel{
			Db: db,
		}

		//1.Query
		log.Printf("\n\n=====01.DB Query測試=====")
		maxOid, err2 := timecardModel.MaxOid()
		if err2 != nil {
			log.Println(err2)
		} else {
			log.Println("Max Oid:", maxOid)
		}

		//2.Delete
		log.Printf("\n\n=====01.DB Delete測試by WEEK_START_DATE+TIMECARD_TYPE+USER_NAME=====")
		rowsAffected_Delete, err2 := timecardModel.DeleteData(20200518, "法程部", "五月天")
		if err2 != nil {
			log.Println(err2)
		} else {
			log.Println("Delete Rows Affected:", rowsAffected_Delete)
		}

		//3.Insert
		log.Printf("\n\n=====02.DB Insert測試=====")
		var vCurrentTime = getCurrentTime()
		nCurrentTime, err := strconv.ParseUint(vCurrentTime, 0, 64)
		if err != nil {
			log.Println(err)
		}
		timecard_Insert := entities.Timecard{
			TIMECARD_YEAR:   2020,
			TIMECARD_MONTH:  5,
			TIMECARD_TYPE:   "H1",
			WEEK_START_DATE: 20200518,
			DAY_OF_WEEK:     1,
			TIMECARD_DATE:   20200519,
			USER_NAME:       "五月天",
			DEPARTMENT:      "法程部",
			WORK_TYPE:       "CT",
			WORK_ITEM:       "2021- 二線客服(ASE)",

			CUSTOMER:     "ASE",
			REQ_NO:       "",
			MANTIS_NO:    "",
			WORK_COMMENT: "KH 換新機器問題(測試區 KH/EE 環境準備+ EE 正式環境準備 11:00~12:00",
			WORK_HOUR:    1.5,
			LAST_DATE:    nCurrentTime,
		}
		rowsAffected_Insert, err2 := timecardModel.CreateData(&timecard_Insert)
		if err2 != nil {
			log.Println(err2)
		} else {
			log.Println("Insert Rows Affected:", rowsAffected_Insert)
			log.Println("timecard Info")
			log.Println("Id:", timecard_Insert.OBJECT_ID)
			log.Println("LAST_DATE:", timecard_Insert.LAST_DATE)
		}

		//4.Update
		log.Printf("\n\n=====03.DB Update測試=====by OBJECT_ID")
		maxOid, err = timecardModel.MaxOid()
		vCurrentTime = getCurrentTime()
		nCurrentTime, err = strconv.ParseUint(vCurrentTime, 0, 64)
		if err != nil {
			log.Println(err)
		}
		timecard_Update := entities.Timecard{
			OBJECT_ID:    maxOid,
			REQ_NO:       "",
			MANTIS_NO:    "",
			WORK_COMMENT: "RD Test Update",
			WORK_HOUR:    3.5,
			LAST_DATE:    nCurrentTime,
		}
		rowsAffected_Update, err2 := timecardModel.UpdateData(&timecard_Update)
		if err2 != nil {
			log.Println(err2)
		} else {
			log.Println("Update Rows Affected:", rowsAffected_Update)
		}
		// for i := 1; i <= 10; i++ {
		//     log.Println(i)
		// }

	}
}
