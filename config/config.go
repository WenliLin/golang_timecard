package config

//go get -u github.com/creamdog/gonfig
import (
	"database/sql"
	"os"

	"github.com/creamdog/gonfig"

	//go get -u github.com/go-sql-driver/mysql

	"log"

	_ "github.com/go-sql-driver/mysql"
)

var (
	vConnectUrl = ""
)

type ExampleMessageStruct struct {
	Message string
	Subject string
}

//回傳DB連線物件 *sql.DB
func GetMySQLDB(filePath string) (db *sql.DB, err error) {
	log.Println("\n\n=====00.讀取設定檔=====")

	f, err := os.Open(filePath)
	if err != nil {
		log.Println("db Open error:", err)
	}
	defer f.Close()
	config, err := gonfig.FromJson(f)
	if err != nil {
		log.Println("db Json error:", err)
	}

	db_type, err := config.GetString("db/type", "null")
	if err != nil {
		log.Println("db/type error:", err)
	}
	db_user_pwd, err := config.GetString("db/user_pwd", "null")
	if err != nil {
		log.Println("db/user error:", err)
	}
	db_url, err := config.GetString("db/url", "null")
	if err != nil {
		log.Println("db/url error:", err)
	}
	dp_owner, err := config.GetString("db/owner", "null")
	if err != nil {
		log.Println("db/owner error:", err)
	}

	vConnectUrl = db_user_pwd + "@tcp(" + db_url + ")/" + dp_owner
	log.Println("\n\n=====01.DB連線測試=====[" + vConnectUrl + "]")
	db, err = sql.Open(db_type, vConnectUrl)

	return
}

//讀取Json檔下面的某個路徑下變數
func GetConfigValue(filePath string, sConfigPath string) (configValue string) {
	log.Println("\n\n=====00.讀取Json=====[File:" + filePath + "][Path:" + sConfigPath + "]")

	f_, err := os.Open(filePath)
	if err != nil {
		log.Println("error Open:", err)
	}
	defer f_.Close()
	config_, err := gonfig.FromJson(f_)
	if err != nil {
		log.Println("error Close:", err)
	}

	//"db/type"
	configValue, err = config_.GetString(sConfigPath, "null")
	if err != nil {
		log.Println("error GetString:", err)
	}

	return
}
