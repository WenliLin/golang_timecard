package models

import (
	"database/sql"

	"../entities"
)

// var {
// 	timecardModel, err := config.GetMySQLDB(os.Getwd() + "/conf.json")
// 	if err != nil {
// 		log.Println(err)
// 	}
// }

type TimecardModel struct {
	Db *sql.DB
}

var (
	table_timecard = "ECO_TIMECARD"
)

//http://learningprogramming.net/category/golang/golang-and-mysql/ SQL教學
func (timecardModel TimecardModel) CreateData(timecard *entities.Timecard) (int64, error) {
	result, err := timecardModel.Db.Exec(
		("insert into " + table_timecard + "(TIMECARD_YEAR,TIMECARD_MONTH,TIMECARD_TYPE,WEEK_START_DATE,DAY_OF_WEEK" + ",TIMECARD_DATE,USER_NAME,DEPARTMENT,WORK_TYPE,WORK_ITEM" + ",CUSTOMER,REQ_NO,MANTIS_NO,WORK_COMMENT,WORK_HOUR" + ",LAST_DATE) " + "values(?,?,?,?,?,  ?,?,?,?,?,  ?,?,?,?,?,  ?)"), timecard.TIMECARD_YEAR, timecard.TIMECARD_MONTH, timecard.TIMECARD_TYPE, timecard.WEEK_START_DATE, timecard.DAY_OF_WEEK, timecard.TIMECARD_DATE, timecard.USER_NAME, timecard.DEPARTMENT, timecard.WORK_TYPE, timecard.WORK_ITEM, timecard.CUSTOMER, timecard.REQ_NO, timecard.MANTIS_NO, timecard.WORK_COMMENT, timecard.WORK_HOUR, timecard.LAST_DATE)
	if err != nil {
		return 0, err
	} else {
		timecard.OBJECT_ID, _ = result.LastInsertId()
		rowsAffected, _ := result.RowsAffected()
		return rowsAffected, nil
	}
}

func (timecardModel TimecardModel) UpdateData(timecard *entities.Timecard) (int64, error) {
	result, err := timecardModel.Db.Exec("update "+table_timecard+" set WORK_COMMENT = ?, WORK_HOUR = ?, REQ_NO = ?, MANTIS_NO= ? where OBJECT_ID = ?", timecard.WORK_COMMENT, timecard.WORK_HOUR, timecard.REQ_NO, timecard.MANTIS_NO, timecard.OBJECT_ID)
	if err != nil {
		return 0, err
	} else {
		return result.RowsAffected()
	}
}

func (timecardModel TimecardModel) DeleteData(WEEK_START_DATE string, DEPARTMENT string, USER_NAME string) (int64, error) {
	result, err := timecardModel.Db.Exec("delete from "+table_timecard+" where WEEK_START_DATE=? and DEPARTMENT = ? and USER_NAME=?", WEEK_START_DATE, DEPARTMENT, USER_NAME)
	if err != nil {
		return 0, err
	} else {
		return result.RowsAffected()
	}
}

func (timecardModel TimecardModel) MaxOid() (int64, error) {
	rows, err := timecardModel.Db.Query("select max(object_id) as max_oid from " + table_timecard)
	if err != nil {
		return 0, err
	} else {
		var max_oid int64
		for rows.Next() {
			rows.Scan(&max_oid)
		}
		return max_oid, nil
	}
}
