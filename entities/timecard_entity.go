package entities

type Timecard struct {
	OBJECT_ID       int64
	TIMECARD_YEAR   int32
	TIMECARD_MONTH  int
	TIMECARD_TYPE   string
	WEEK_START_DATE string

	DAY_OF_WEEK   int
	TIMECARD_DATE string
	USER_NAME     string
	DEPARTMENT    string
	WORK_TYPE     string

	WORK_ITEM    string
	CUSTOMER     string
	REQ_NO       string
	MANTIS_NO    string
	WORK_COMMENT string

	WORK_HOUR float64
	LAST_DATE uint64
}
