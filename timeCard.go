﻿package main

//使用教學:
//安裝go 1.14.3
//go get -u -v github.com/ramya-rao-a/go-outline
//go get -u -v github.com/acroca/go-symbols
//go get -u -v github.com/mdempsky/gocode
//go get -u -v github.com/rogpeppe/godef
//go get -u -v golang.org/x/tools/cmd/godoc
//go get -u -v github.com/zmb3/gogetdoc
//go get -u -v golang.org/x/lint/golint
//go get -u -v github.com/fatih/gomodifytags
//go get -u -v golang.org/x/tools/cmd/gorename
//go get -u -v sourcegraph.com/sqs/goreturns
//go get -u -v golang.org/x/tools/cmd/goimports
//go get -u -v github.com/cweill/gotests/...
//go get -u -v golang.org/x/tools/cmd/guru
//go get -u -v github.com/josharian/impl
//go get -u -v github.com/haya14busa/goplay/cmd/goplay
//go get -u -v github.com/uudashr/gopkgs/cmd/gopkgs
//go get -u -v github.com/davidrjenni/reftools/cmd/fillstruct

// read excel
//go get github.com/Luxurioust/excelize
// read sql  gonfig
//go get -u github.com/creamdog/gonfig
// read sql  mysql
//go get -u github.com/go-sql-driver/mysql
import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"./config"
	"./entities"
	"./models"
	"github.com/tealeg/xlsx"
)

const (
	layout = "2006-01-02 15:04:05"
)

//0-1.設定 log目錄
var (
	t                  = time.Now()
	logger             *log.Logger
	formatted          = ""
	configPath         = os.Args[1] // XXXXX/conf.json<-- exe 輸出使用
	logPath            = config.GetConfigValue(configPath, "dir/Log/Path")
	vPath              = config.GetConfigValue(configPath, "dir/Timecard/Path")
	vFolder            = config.GetConfigValue(configPath, "dir/Timecard/Folder")
	vFileNameExtension = config.GetConfigValue(configPath, "dir/Timecard/FileNameExtension")
)

func main() {
	fmt.Println(">>>>go is  Start : " + getCurrentTime())

	// logPath does not exist
	if _, err := os.Stat(logPath); os.IsNotExist(err) {
		os.MkdirAll(logPath, os.ModePerm)
	}

	//設定 Log File Setting
	var vCurrentDT = getCurrentDate()
	//O_RDONLY 	:唯讀/O_WRONLY :唯寫/O_RDWR: 讀寫
	//O_APPEND 	:寫入時使用附加方式
	//O_CREATE 	:檔案不存在時建立新檔
	//O_EXCL   	:與 O_CREATE 併用，檔案必須不存在
	//O_SYNC 	:以同步 I/O 開啟
	//O_TRUNC  	:檔案開啟時清空文件
	//perm 的話是檔案八進位權限，例如 0777；另外，還有個 os.Create，實現上就是使用 OpenFile 以 0666 的方式建立可讀寫的檔案（清空文件）：
	f, err := os.OpenFile(logPath+"/Timcard_log_"+vCurrentDT+".log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()
	log.SetOutput(f)

	//更新os 預設path
	os.Chdir(vPath)
	log.Println(os.Getwd())

	//Read Folder Number
	iFileNumber := 0
	iDoFileNumber := 0
	filepath.Walk("./"+vFolder, func(path string, info os.FileInfo, err error) error {
		// only read vFileNameExtension and not read tempFile(ex:~$ )
		if strings.Contains(path, vFileNameExtension) && !strings.Contains(path, "~$") {
			iFileNumber = iFileNumber + 1
		}
		if err != nil {
			log.Println("error Read Folder  "+vPath+"/"+vFolder+" :  ", err)
			fmt.Println("error Read Folder  "+vPath+"/"+vFolder+" :  ", err)
		}
		return nil
	})

	filepath.Walk("./"+vFolder, func(path string, info os.FileInfo, err error) error {
		// only read vFileNameExtension and not read tempFile(ex:~$ )
		if strings.Contains(path, vFileNameExtension) && !strings.Contains(path, "~$") {
			iDoFileNumber = iDoFileNumber + 1
			log.Println("(%v/%v)Name: %s, ModifyTime: %s, Size: %v\n", iDoFileNumber, iFileNumber, path, info.ModTime().Format(layout), info.Size())
			fmt.Printf("(%v/%v)Name: %s, ModifyTime: %s, Size: %v\n", iDoFileNumber, iFileNumber, path, info.ModTime().Format(layout), info.Size())
			readExcelFile(path)
		}
		return nil
	})
	fmt.Println(">>>>go is Finish : " + getCurrentTime())

}

func readExcelFile(path string) {
	db, err := config.GetMySQLDB(configPath)
	if err != nil {
		log.Println(err)
	}
	timecardModel := models.TimecardModel{
		Db: db,
	}

	// get LAST_DATE
	var vCurrentTime = getCurrentTime()
	sLAST_DATE, err := strconv.ParseUint(vCurrentTime, 0, 64)
	log.Printf(">>>>>sLAST_DATE=%s\t", sLAST_DATE)

	var lnWeeks [7]int
	var lnDays [7]string
	var sDEPARTMENT = ""
	var sWEEK_START_DATE = ""
	var sUSER_NAME = ""

	path = strings.Replace(path, "\\", "/", -1)
	xlFile, err := xlsx.OpenFile(vPath + "/" + path) // Open File

	if err != nil {
		log.Printf("open failed: %s\n", err)
		return
	}

	for _, sheet := range xlFile.Sheets {
		if strings.Contains(sheet.Name, ".") { // sheet Name 卡控
			log.Printf("Sheet Name=" + sheet.Name)
			fmt.Println("Sheet Name=" + sheet.Name)

			sBlack := false // Stop read Sheet Row
			for iRow := 1; iRow < sheet.MaxRow; iRow++ {
				row, err := sheet.Row(iRow)
				if err != nil {
					log.Printf("open failed: %s\n", err)
					return
				}
				log.Println(">>iRow=%d", iRow)

				switch iRow {
				case 1:
					sDEPARTMENT = row.GetCell(1).String()                          //部門
					sWEEK_START_DATE = convertToFormatDay(row.GetCell(7).String()) //週起日
				case 2:
					sUSER_NAME = row.GetCell(1).String() //姓名
					//2.Delete
					log.Printf("\n\n=====01.DB Delete測試by WEEK_START_DATE+TIMECARD_TYPE+USER_NAME=====")
					rowsAffected_Delete, err2 := timecardModel.DeleteData(sWEEK_START_DATE, sDEPARTMENT, sUSER_NAME)
					if err2 != nil {
						log.Println(err2)
					} else {
						log.Println("Delete Rows Affected:", rowsAffected_Delete)
					}
				case 3, 4:
					for iCell := 6; iCell < 13; iCell++ { //G:6~K:13行
						text := row.GetCell(iCell) //日期
						idays := iCell - 6

						if iRow == 3 {
							lnDays[idays] = convertToFormatDay(text.String())
						} else {
							lnWeeks[idays] = idays + 1 //1(一)~7(日)
						}
					}
				default:
					sWORK_TYPE := row.GetCell(0).String()    //類別
					sWORK_ITEM := row.GetCell(1).String()    //工作項目
					sCUSTOMER := row.GetCell(2).String()     //客戶名稱
					sREQ_NO := row.GetCell(3).String()       //Req. No.
					sMANTIS_NO := row.GetCell(4).String()    //Mantis No.
					sWORK_COMMENT := row.GetCell(5).String() //工作說明
					if err != nil {
						log.Println(err)
					}

					if sWORK_TYPE != "" && sWORK_ITEM != "" {
						for iCell := 6; iCell < 13; iCell++ { //G:6~K:13行  iCell < row.Sheet.MaxCol;
							idays := iCell - 6
							sWORK_HOUR := row.GetCell(iCell).String() //工時(小時)
							flWORK_HOUR, err := strconv.ParseFloat(sWORK_HOUR, 64)
							sTIMECARD_DATE := lnDays[idays]
							sTIMECARD_YEAR := sTIMECARD_DATE[0:4]
							iTIMECARD_YEAR, err := strconv.ParseInt(sTIMECARD_YEAR, 10, 32)
							sTIMECARD_MONTH := sTIMECARD_DATE[5:7]
							iTIMECARD_MONTH, err := strconv.Atoi(sTIMECARD_MONTH)
							iMonth, err := strconv.Atoi(sTIMECARD_MONTH) // result: int
							if err != nil {
								log.Println(err)
							}
							sTIMECARD_TYPE := "H1" // MONTH >= 7 月H2
							if iMonth >= 7 {
								sTIMECARD_TYPE = "H2"
							}

							if sWORK_HOUR != "" {
								//insert Data Hr
								timecard_Insert := entities.Timecard{
									TIMECARD_YEAR:   int32(iTIMECARD_YEAR),
									TIMECARD_MONTH:  iTIMECARD_MONTH,
									TIMECARD_TYPE:   sTIMECARD_TYPE,
									WEEK_START_DATE: sWEEK_START_DATE,
									DAY_OF_WEEK:     lnWeeks[idays],

									TIMECARD_DATE: sTIMECARD_DATE,
									USER_NAME:     sUSER_NAME,
									DEPARTMENT:    sDEPARTMENT,
									WORK_TYPE:     sWORK_TYPE,
									WORK_ITEM:     sWORK_ITEM,

									CUSTOMER:     sCUSTOMER,
									REQ_NO:       sREQ_NO,
									MANTIS_NO:    sMANTIS_NO,
									WORK_COMMENT: sWORK_COMMENT,
									WORK_HOUR:    flWORK_HOUR,

									LAST_DATE: sLAST_DATE,
								}

								rowsAffected_Insert, err2 := timecardModel.CreateData(&timecard_Insert)
								if err2 != nil {
									log.Println(err2)
								} else {
									log.Println("Insert Rows Affected:", rowsAffected_Insert)
									log.Println("timecard Info")
									log.Println("Id:", timecard_Insert.OBJECT_ID)
									log.Println("LAST_DATE:", timecard_Insert.LAST_DATE)
								}

							}
						}
					} else if (sWORK_TYPE == "" && sWORK_ITEM == "小計") || sWORK_TYPE == "說明：" {
						sBlack = true // Stop read Sheet Row
					}

				}
				if sBlack { // Stop read Sheet Row
					break
				}
			}

		}
	}

}

func convertToFormatDay(excelDaysString string) string {
	f, err := strconv.ParseFloat(excelDaysString, 64)
	if err != nil {
		log.Printf("open failed: %s\n", err)
	}
	date := xlsx.TimeFromExcelTime(f, false)
	return date.Format("2006-01-02")
}

func getCurrentTime() string {
	t = time.Now()
	//YYYYMMDDTHHMMSS
	formatted = fmt.Sprintf("%d%02d%02d%02d%02d%02d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
	return formatted
}

func getCurrentDate() string {
	t = time.Now()
	//YYYYMMDD
	formatted = fmt.Sprintf("%d%02d%02d", t.Year(), t.Month(), t.Day())
	return formatted
}
